import { ClientLevelError } from '../../errors';

export class UserWithEmailAlreadyExistsException extends ClientLevelError {
  constructor() {
    super('ERR_USER_WITH_EMAIL_ALREADY_EXISTS');
  }
}

export class UserWithEmailNotFoundException extends ClientLevelError {
  constructor() {
    super('ERR_USER_WITH_EMAIL_NOT_FOUND');
  }
}

export class UserPasswordNotValidException extends ClientLevelError {
  constructor() {
    super('ERR_USER_PASSWORD_NOT_VALID');
  }
}

export class UserDoesNotExistsException extends ClientLevelError {
  constructor() {
    super('ERR_USER_NOT_FOUND');
  }
}
