import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { UsersService } from './users.service';
import { GetUserNameRequestDTO, LoginRequestDTO, LoginResponseDTO, SignUpRequestDTO } from './users.dto';
import { Empty } from '../../utils/utils.interfaces';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post('signUp')
  async signUp(@Body() data: SignUpRequestDTO): Promise<Empty> {
    await this.usersService.signUp(data);
    return {};
  }

  @Post('login')
  async login(@Body() data: LoginRequestDTO): Promise<LoginResponseDTO> {
    return await this.usersService.login(data);
  }

  @Get('getUserName')
  async getUserName(@Query() { userId }: GetUserNameRequestDTO): Promise<string> {
    return await this.usersService.getUserName(userId);
  }
}
