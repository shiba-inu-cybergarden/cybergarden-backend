import { IsEmail, IsString, Length, Matches, MinLength } from 'class-validator';

export class SignUpRequestDTO {
  @IsString()
  @IsEmail()
  email!: string;

  @IsString()
  @MinLength(6)
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, { message: 'password too weak :D' })
  password!: string;

  @IsString()
  @Length(1, 20)
  name!: string;
}

export class LoginRequestDTO {
  @IsString()
  email!: string;

  @IsString()
  password!: string;
}

export class LoginResponseDTO {
  userId: string;
  userName: string;
}

export class GetUserNameRequestDTO {
  @IsString()
  userId!: string;
}
