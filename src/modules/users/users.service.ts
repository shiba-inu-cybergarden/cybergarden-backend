import { InjectRepository } from '@nestjs/typeorm';
import { Injectable } from '@nestjs/common';
import { UserModelEntity } from '../../data-sources';
import { Repository } from 'typeorm';
import { LoginRequestDTO, LoginResponseDTO, SignUpRequestDTO } from './users.dto';
import {
  UserDoesNotExistsException,
  UserPasswordNotValidException,
  UserWithEmailAlreadyExistsException,
  UserWithEmailNotFoundException,
} from './users.errors';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserModelEntity)
    private readonly usersRepository: Repository<UserModelEntity>,
  ) {}

  async signUp({ email, name, password }: SignUpRequestDTO): Promise<void> {
    const emailChanged = email.toLowerCase();
    if (await this.isByEmail(emailChanged)) {
      throw new UserWithEmailAlreadyExistsException();
    }
    const hashedPassword = await bcrypt.hash(password, 10);
    await this.usersRepository.save({
      email: emailChanged,
      name,
      password: hashedPassword,
    });
  }

  async login({ email, password }: LoginRequestDTO): Promise<LoginResponseDTO> {
    const currentUser = await this.getAuthenticatedUser(email, password);
    return {
      userId: currentUser.id,
      userName: currentUser.name,
    };
  }

  async getUserName(userId: string): Promise<string> {
    const foundUser = await this.usersRepository.findOne(userId);
    if (!foundUser) {
      throw new UserDoesNotExistsException();
    }
    return foundUser.name;
  }

  private async isByEmail(email: string): Promise<boolean> {
    const user = await this.usersRepository.findOne({ where: { email } });
    return !!user;
  }

  private async getAuthenticatedUser(email: string, plainTextPassword: string): Promise<UserModelEntity> {
    const user = await this.getByEmail(email);
    await this.verifyPassword(plainTextPassword, user.password);
    return user;
  }

  private async getByEmail(email: string): Promise<UserModelEntity> {
    const user = await this.usersRepository.findOne({ where: { email } });
    if (!user) {
      throw new UserWithEmailNotFoundException();
    }
    return user;
  }

  private async verifyPassword(plainTextPassword: string, hashedPassword: string): Promise<void> {
    if (hashedPassword) {
      const isPasswordMatching = await bcrypt.compare(plainTextPassword, hashedPassword);
      if (!isPasswordMatching) {
        throw new UserPasswordNotValidException();
      }
    }
  }
}
