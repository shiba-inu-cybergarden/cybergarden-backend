import { InnerLevelError } from '../../errors';

export class InvalidConfig extends InnerLevelError {
  constructor(customMessage?: string) {
    const code = 'ERR_INVALID_CONFIG';
    const message = 'Environment configuration is invalid';
    super(code, customMessage || message);
  }
}
