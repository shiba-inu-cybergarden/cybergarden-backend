import { Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validateSync } from 'class-validator';

import { ConfigModel } from './config.model';
import { InvalidConfig } from './config.errors';
import 'dotenv/config';

@Injectable()
export class ConfigService {
  private readonly _config: ConfigModel;

  constructor() {
    this._config = ConfigService.validateConfig();
  }

  get config(): ConfigModel {
    return this._config;
  }

  private static validateConfig(): ConfigModel {
    const envs = process.env as any;
    const plainConfig = {
      env: envs.ENV,
      db: {
        host: envs.DB_HOST,
        port: Number(envs.DB_PORT),
        username: envs.DB_USERNAME,
        password: envs.DB_PASSWORD,
        database: envs.DB_DATABASE,
        dropSchema: Boolean(envs.DB_DROP_SCHEMA),
        driverExtra: envs.DB_DRIVER_EXTRA,
      },
    };
    const result = plainToClass(ConfigModel, plainConfig);
    const errors = validateSync(result);
    if (errors.length > 0) {
      const list = errors.map((error) => {
        return error.toString();
      });
      throw new InvalidConfig(`Environment configuration is invalid: ` + list.join(' - '));
    }

    return result;
  }
}
