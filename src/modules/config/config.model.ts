import { IsBoolean, IsEnum, IsNumber, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';

export class DatabaseSettings {
  @IsString()
  host!: string;

  @IsNumber()
  port!: number;

  @IsString()
  username!: string;

  @IsString()
  password!: string;

  @IsString()
  database!: string;

  @IsBoolean()
  dropSchema!: boolean;

  @IsString()
  driverExtra!: string;
}

export enum EnvEnums {
  TESTS = 'TESTS',
  DEVELOP = 'DEVELOP',
  PROD = 'PROD',
}

export class ConfigModel {
  @IsEnum(EnvEnums)
  readonly env!: EnvEnums;

  @ValidateNested()
  @Type(() => DatabaseSettings)
  readonly db!: DatabaseSettings;
}
