import { Body, Controller, Get, Post } from '@nestjs/common';
import { MapZoneService } from './mapZone.service';
import {
  getAllMapZoneResponseDTO,
  GetZoneInfoByCoordinatesRequestDTO,
  GetZoneInfoByCoordinatesResponseDTO,
  SetMapZoneCoordinatesRequestDTO
} from "./mapZone.dto";
import { Empty } from '../../utils/utils.interfaces';

@Controller()
export class mapZoneController {
  constructor(private readonly mapZoneService: MapZoneService) {}

  @Post('setMapZone')
  async setMapZone(@Body() data: SetMapZoneCoordinatesRequestDTO): Promise<Empty> {
    await this.mapZoneService.setMapZone(data);
    return {};
  }

  @Get('getAllMapZone')
  async getAllMapZone(): Promise<getAllMapZoneResponseDTO> {
    return await this.mapZoneService.getAllMapZone();
  }

  @Post('getZoneInfoByCoordinates')
  async getZoneInfoByCoordinates(
    @Body() data: GetZoneInfoByCoordinatesRequestDTO,
  ): Promise<GetZoneInfoByCoordinatesResponseDTO> {
    return await this.mapZoneService.getZoneInfoByCoordinates(data);
  }
}
