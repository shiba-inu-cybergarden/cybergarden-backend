export interface GeolibCoordinatesInterface {
  latitude: number;
  longitude: number;
}
