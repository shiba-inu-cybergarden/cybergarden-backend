import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { mapZoneController } from './mapZone.controller';
import { MapZoneService } from './mapZone.service';
import { MapZoneModelEntity } from "../../data-sources";

@Module({
  imports: [TypeOrmModule.forFeature([MapZoneModelEntity])],
  controllers: [mapZoneController],
  providers: [MapZoneService],
  exports: [MapZoneService],
})
export class mapZoneModule {}
