import { ClientLevelError } from "../../errors";

export class PointCoordinatesDoesntMatchAnyZoneException extends ClientLevelError {
  constructor() {
    super('ERR_POINT_COORDINATES_DOESNT_MATCH_ANY_ZONE');
  }
}
