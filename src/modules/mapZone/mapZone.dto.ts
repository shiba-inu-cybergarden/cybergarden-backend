import { MapZoneModelEntity } from '../../data-sources';

export class SetMapZoneCoordinatesRequestDTO {
  coordinates!: [number[]];
  floor!: number;
  zoneName!: string;
}

export class getAllMapZoneResponseDTO {
  mapZoneData: MapZoneModelEntity[];
}

export class GetZoneInfoByCoordinatesRequestDTO {
  coordinates!: number[];
}

export class GetZoneInfoByCoordinatesResponseDTO {
  floor: number;
  zoneName: string;
}
