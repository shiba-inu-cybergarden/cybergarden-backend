import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { MapZoneModelEntity } from '../../data-sources';
import {
  getAllMapZoneResponseDTO,
  GetZoneInfoByCoordinatesRequestDTO,
  GetZoneInfoByCoordinatesResponseDTO,
  SetMapZoneCoordinatesRequestDTO,
} from './mapZone.dto';
import * as geolib from 'geolib';
import { GeolibCoordinatesInterface } from './mapZone.interfaces';
import { PointCoordinatesDoesntMatchAnyZoneException } from './mapZone.errors';

@Injectable()
export class MapZoneService {
  constructor(
    @InjectRepository(MapZoneModelEntity)
    private readonly mapZoneRepository: Repository<MapZoneModelEntity>,
  ) {}

  async setMapZone(data: SetMapZoneCoordinatesRequestDTO): Promise<void> {
    await this.mapZoneRepository.save(data);
  }

  async getAllMapZone(): Promise<getAllMapZoneResponseDTO> {
    return {
      mapZoneData: await this.mapZoneRepository.find(),
    };
  }

  async getZoneInfoByCoordinates(
    data: GetZoneInfoByCoordinatesRequestDTO,
  ): Promise<GetZoneInfoByCoordinatesResponseDTO> {
    const foundAllZones = (await this.getAllMapZone()).mapZoneData;
    for (const zone of foundAllZones) {
      const geolibZoneCoordinates: GeolibCoordinatesInterface[] = zone.coordinates.map((i) => {
        return {
          latitude: i[0],
          longitude: i[1],
        };
      });

      const geolibPointCoordinates: GeolibCoordinatesInterface = {
        latitude: data.coordinates[0],
        longitude: data.coordinates[1],
      };
      if (geolib.isPointInPolygon(geolibPointCoordinates, geolibZoneCoordinates)) {
        return {
          floor: zone.floor,
          zoneName: zone.zoneName,
        };
      }
    }

    throw new PointCoordinatesDoesntMatchAnyZoneException();
  }
}
