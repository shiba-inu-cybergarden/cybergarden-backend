import { ClientLevelError } from '../../errors';

export class DeviceByIdNotFoundException extends ClientLevelError {
  constructor() {
    super('ERR_DEVICE_BY_ID_NOT_FOUND');
  }
}

export class DeviceIsNotAvailableToTakeException extends ClientLevelError {
  constructor() {
    super('ERR_DEVICE_IS_NOT_AVAILABLE_TO_TAKE');
  }
}

export class DeviceAlreadyAvailableNowException extends ClientLevelError {
  constructor() {
    super('ERR_DEVICE_ALREADY_AVAILABLE_NOW');
  }
}

export class DeviceMustBeenAuthorizedException extends ClientLevelError {
  constructor() {
    super('ERR_DEVICE_MUST_BEEN_AUTHORIZED');
  }
}

export class DeviceCanOnlyBeReturnedWhoUseItException extends ClientLevelError {
  constructor() {
    super('ERR_DEVICE_CAN_ONLY_BE_RETURNED_WHO_USE_IT');
  }
}
