import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DevicesController } from './devices.controller';
import { DevicesService } from './devices.service';
import { DeviceModelEntity, UserModelEntity } from "../../data-sources";

@Module({
  imports: [TypeOrmModule.forFeature([DeviceModelEntity, UserModelEntity])],
  controllers: [DevicesController],
  providers: [DevicesService],
  exports: [DevicesService],
})
export class DevicesModule {}
