import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { DevicesService } from './devices.service';
import {
  CreateDeviceRequestDTO,
  CreateDeviceResponseDTO,
  DeleteDeviceRequestDTO,
  UpdateDeviceRequestDTO,
  GetAllDevicesResponseDTO,
  VerifyAuthCodeRequestDTO,
  VerifyAuthCodeResponseDTO,
  SetDeviceDynamicPropertyRequestDTO,
  TakeDeviceRequestDTO,
  GetDeviceRequestDTO,
  PutDeviceBackRequestDTO,
} from './devices.dto';
import { Empty } from '../../utils/utils.interfaces';
import { DeviceModelEntity } from '../../data-sources';

@Controller()
export class DevicesController {
  constructor(private readonly devicesService: DevicesService) {}

  @Get('getAllDevices')
  async getAllDevices(): Promise<GetAllDevicesResponseDTO> {
    return await this.devicesService.getAllDevices();
  }

  @Get('getDevice')
  async getDevice(@Query() { deviceId }: GetDeviceRequestDTO): Promise<DeviceModelEntity> {
    return await this.devicesService.getDevice(deviceId);
  }

  @Post('createDevice')
  async createDevice(@Body() data: CreateDeviceRequestDTO): Promise<CreateDeviceResponseDTO> {
    return this.devicesService.createDevice(data);
  }

  @Post('updateDevice')
  async updateDevice(@Body() data: UpdateDeviceRequestDTO): Promise<Empty> {
    await this.devicesService.updateDevice(data);
    return {};
  }

  @Post('deleteDevice')
  async deleteDevice(@Body() data: DeleteDeviceRequestDTO): Promise<Empty> {
    await this.devicesService.deleteDevice(data);
    return {};
  }

  @Get('verifyAuthCode')
  async verifyAuthCode(@Query() data: VerifyAuthCodeRequestDTO): Promise<VerifyAuthCodeResponseDTO> {
    return await this.devicesService.verifyAuthCode(data.pinCode);
  }

  @Post('setDeviceDynamicProperty')
  async setDeviceDynamicProperty(@Body() data: SetDeviceDynamicPropertyRequestDTO): Promise<Empty> {
    await this.devicesService.setDeviceDynamicProperty(data);
    return {};
  }

  @Post('takeDevice')
  async takeDevice(@Body() data: TakeDeviceRequestDTO): Promise<Empty> {
    await this.devicesService.takeDevice(data);
    return {};
  }

  @Post('putDeviceBack')
  async putDeviceBack(@Body() data: PutDeviceBackRequestDTO): Promise<Empty> {
    await this.devicesService.putDeviceBack(data);
    return {};
  }
}
