import { Injectable } from '@nestjs/common';
import { DeviceModelEntity, UserModelEntity } from '../../data-sources';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import {
  CreateDeviceRequestDTO,
  CreateDeviceResponseDTO,
  DeleteDeviceRequestDTO,
  GetAllDevicesResponseDTO,
  PutDeviceBackRequestDTO,
  SetDeviceDynamicPropertyRequestDTO,
  TakeDeviceRequestDTO,
  UpdateDeviceRequestDTO,
  VerifyAuthCodeResponseDTO,
} from './devices.dto';
import * as securePin from 'secure-pin';
import {
  DeviceAlreadyAvailableNowException,
  DeviceByIdNotFoundException,
  DeviceCanOnlyBeReturnedWhoUseItException,
  DeviceIsNotAvailableToTakeException,
  DeviceMustBeenAuthorizedException,
} from './devices.errors';
import { getNowTimeInMilliseconds } from '../../utils/utils';
import { UserDoesNotExistsException } from "../users/users.errors";

@Injectable()
export class DevicesService {
  constructor(
    @InjectRepository(DeviceModelEntity)
    private readonly devicesRepository: Repository<DeviceModelEntity>,
    @InjectRepository(UserModelEntity)
    private readonly usersRepository: Repository<UserModelEntity>,
  ) {}

  async getAllDevices(): Promise<GetAllDevicesResponseDTO> {
    return {
      devicesData: await this.devicesRepository.find(),
    };
  }

  async getDevice(deviceId: string): Promise<DeviceModelEntity> {
    const foundDevice = await this.devicesRepository.findOne(deviceId);
    if (!foundDevice) {
      throw new DeviceByIdNotFoundException();
    }
    return foundDevice;
  }

  async createDevice(device: CreateDeviceRequestDTO): Promise<CreateDeviceResponseDTO> {
    const randomPinCode = securePin.generatePinSync(4);

    await this.devicesRepository.save({
      ...device,
      deviceAuthCode: randomPinCode,
    });

    return { deviceAuthCode: randomPinCode };
  }

  async verifyAuthCode(pinCode: string): Promise<VerifyAuthCodeResponseDTO> {
    const foundDevice = await this.devicesRepository.findOne({ where: { deviceAuthCode: pinCode } });
    if (!foundDevice) return { result: false };

    await this.devicesRepository.update(foundDevice, { isAuthComplete: true });
    return {
      result: true,
      deviceId: foundDevice.id,
    };
  }

  async setDeviceDynamicProperty(data: SetDeviceDynamicPropertyRequestDTO): Promise<void> {
    const foundDevice = await this.existDevice(data.deviceId);
    await this.devicesRepository.update(foundDevice.id, {
      deviceBrandName: data.deviceBrandName,
      deviceModel: data.deviceModel,
      deviceOS: data.deviceOS,
      coordinates: data.coordinates,
      batteryCharge: Math.floor(data.batteryCharge * 100),
      height: data.height,
    });
  }

  async updateDevice(data: UpdateDeviceRequestDTO): Promise<void> {
    const foundDevice = await this.existDevice(data.deviceId);

    const dataToUpdate: Partial<DeviceModelEntity> = {
      deviceModel: data.deviceModel,
      deviceName: data.deviceName,
      deviceBrandName: data.deviceBrandName,
      deviceOS: data.deviceOS,
    };
    await this.devicesRepository.update(foundDevice, dataToUpdate);
  }

  async deleteDevice(data: DeleteDeviceRequestDTO): Promise<void> {
    await this.existDevice(data.deviceId);
    await this.devicesRepository.delete(data.deviceId);
  }

  async takeDevice({ userId, deviceId }: TakeDeviceRequestDTO): Promise<void> {
    const foundDevice = await this.devicesRepository.findOne(deviceId);
    if (!foundDevice) {
      throw new DeviceByIdNotFoundException();
    }
    if (!foundDevice.isAvailable) {
      throw new DeviceIsNotAvailableToTakeException();
    }
    if (!foundDevice.isAuthComplete) {
      throw new DeviceMustBeenAuthorizedException();
    }

    const userData = await this.usersRepository.findOne(userId);
    if (!userData) {
      throw new UserDoesNotExistsException();
    }

    console.log('userId: ', userId);
    console.log('userData: ', userData);
    const dataToUpdate: Partial<DeviceModelEntity> = {
      userIdTakenDevice: userId,
      userNameTakenDevice: userData.name,
      isAvailable: false,
    };
    console.log('dataToUpdate: ', dataToUpdate);
    console.log('foundDevice.id: ', foundDevice.id);
    // TODO: сделать добавление записи в историю у пользователя
    await this.devicesRepository.update(foundDevice.id, dataToUpdate);
  }

  async putDeviceBack({ userId, deviceId }: PutDeviceBackRequestDTO): Promise<void> {
    const foundDevice = await this.devicesRepository.findOne(deviceId);
    if (!foundDevice) {
      throw new DeviceByIdNotFoundException();
    }
    if (foundDevice.isAvailable) {
      throw new DeviceAlreadyAvailableNowException();
    }
    if (foundDevice.userIdTakenDevice !== userId) {
      throw new DeviceCanOnlyBeReturnedWhoUseItException();
    }

    const dataToUpdate: Partial<DeviceModelEntity> = {
      userIdTakenDevice: null,
      userNameTakenDevice: null,
      isAvailable: true,
      lastTimeDeviceReturned: getNowTimeInMilliseconds(),
    };
    // TODO: сделать обновление записи в историю у пользователя
    await this.devicesRepository.update(foundDevice.id, dataToUpdate);
  }

  private async existDevice(deviceId: string): Promise<DeviceModelEntity> {
    const foundDevice = await this.devicesRepository.findOne(deviceId);
    if (!foundDevice) {
      throw new DeviceByIdNotFoundException();
    }
    return foundDevice;
  }
}
