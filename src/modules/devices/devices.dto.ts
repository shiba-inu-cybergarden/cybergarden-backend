import { DeviceModelEntity } from '../../data-sources';
import { IsNumber, IsString } from 'class-validator';

export class GetAllDevicesResponseDTO {
  devicesData: DeviceModelEntity[];
}

export class CreateDeviceRequestDTO {
  @IsString()
  deviceName!: string;
}

export class CreateDeviceResponseDTO {
  deviceAuthCode: string;
}

export class VerifyAuthCodeRequestDTO {
  @IsString()
  pinCode: string;
}

export class VerifyAuthCodeResponseDTO {
  result: boolean;
  deviceId?: string;
}

export class SetDeviceDynamicPropertyRequestDTO {
  @IsString()
  deviceId!: string;

  @IsString()
  deviceBrandName!: string;

  @IsString()
  deviceModel!: string;

  @IsString()
  deviceOS!: string;

  @IsNumber({}, { each: true })
  coordinates!: number[];

  @IsNumber()
  batteryCharge!: number;

  @IsString()
  height?: string;
}

export class UpdateDeviceRequestDTO {
  @IsString()
  deviceId: string;

  @IsString()
  deviceName?: string;

  @IsString()
  deviceBrandName?: string;

  @IsString()
  deviceModel?: string;

  @IsString()
  deviceOS?: string;
}

export class DeleteDeviceRequestDTO {
  @IsString()
  deviceId!: string;
}

export class TakeDeviceRequestDTO {
  @IsString()
  deviceId!: string;

  @IsString()
  userId!: string;
}

export class GetDeviceRequestDTO {
  @IsString()
  deviceId!: string;
}

export class PutDeviceBackRequestDTO {
  @IsString()
  deviceId!: string;

  @IsString()
  userId!: string;
}
