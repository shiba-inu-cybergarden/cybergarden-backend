export function getNowTimeInMilliseconds(): string {
  const currentDate = new Date();
  return currentDate.getTime().toFixed();
}
