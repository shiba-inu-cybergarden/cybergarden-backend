export enum messagesErrors {
  ERR_UNKNOWN_SERVER_ERROR= 'Ошибка на сервере',
  ERR_VALIDATION_FAILED = 'Ошибка валидации',
  ERR_DATABASE_CONFIG_VALIDATE_ERROR = 'Ошибка валидации настроек для подключения к базе данных',
  ERR_CAR_NOT_FOUND = 'Ошибка - детали о машине не найдены',
  ERR_PLATE_VALIDATION_FAILED = 'Ошибка - государственный номер автомобиля не корректный',

  ERR_USER_NOT_FOUND = 'Ошибка - пользователь не найден',
  ERR_USER_WITH_EMAIL_NOT_FOUND = 'Ошибка - пользователь с указанной почтой не найден',
  ERR_USER_PASSWORD_NOT_VALID = 'Ошибка - пароль не корректный',
  ERR_USER_WITH_EMAIL_ALREADY_EXISTS = 'Ошибка - пользователь с такой почтой уже существует',
  ERR_EMAIL_NOT_CONFIRMED = 'Ошибка - электронная почта не была подтверждена',

  ERR_OFFICE_NOT_FOUND = 'Ошибка - офис не найден по указанному имени',
  ERR_DEVICE_BY_ID_NOT_FOUND = 'Ошибка - устройство не найдено по указанному deviceId',
  ERR_DEVICE_IS_NOT_AVAILABLE_TO_TAKE = 'Ошибка - устройство уже кем-то занято',
  ERR_DEVICE_ALREADY_AVAILABLE_NOW = 'Ошибка - использование устройством уже было завершено ранее, сейчас устройство свободно для пользования',
  ERR_DEVICE_MUST_BEEN_AUTHORIZED = 'Ошибка - устройство прежде чем использоваться должно быть авторизовано',
  ERR_DEVICE_CAN_ONLY_BE_RETURNED_WHO_USE_IT = 'Ошибка - устройство может быть возвращено только тем, кто его использует',
  ERR_POINT_COORDINATES_DOESNT_MATCH_ANY_ZONE = 'Ошибка - не удалось найти ни одной зоны координат, для указанной точки',
}
