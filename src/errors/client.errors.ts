import { CustomError } from 'ts-custom-error';

export class ClientLevelError extends CustomError {
  public constructor(public code: string, public subst?: any[], public text?: string) {
    super();
  }
}

export class UnknownServerError extends ClientLevelError {
  constructor() {
    super('ERR_UNKNOWN_SERVER_ERROR');
  }
}

export class ValidationErrors extends ClientLevelError {
  constructor() {
    super('ERR_VALIDATION_FAILED');
  }
}
