import { CustomError } from 'ts-custom-error';

export class InnerLevelError extends CustomError {
  public constructor(
    public code: string,
    message: string,
  ) {
    super(message);
  }
}
