import { ClientLevelError } from '../errors';

export function okResponse(payload: any) {
  return makeResponse('ok', '', '', payload);
}

function makeResponse(status: string, message: string, code: string, payload: any) {
  return {
    status,
    message,
    code,
    payload,
  };
}

export function errorResponse(error: ClientLevelError) {
  let message = error.text ? error.text : 'Unknown error';
  if (error.subst?.length) {
    for (let i = 0; i < error.subst.length; i++) {
      const pattern = new RegExp('#' + i, 'g');
      message = message.replace(pattern, error.subst[i]);
    }
  }
  return makeResponse('error', message, error.code, {});
}
