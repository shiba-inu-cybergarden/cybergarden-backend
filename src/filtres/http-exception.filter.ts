import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { ClientLevelError, UnknownServerError } from '../errors';
import { errorResponse } from './responses';
import { messagesErrors } from '../errors/messages.errors';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
  public catch(exception: any, host: ArgumentsHost): void {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    const exceptionCode = exception.code as keyof typeof messagesErrors;
    exception.text = messagesErrors[exceptionCode];
    if (!(exception instanceof ClientLevelError)) {
      console.error('UnknownServerError occurred: ', exception);
      exception = new UnknownServerError();
    }
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    response.json(errorResponse(exception as ClientLevelError));
  }
}
