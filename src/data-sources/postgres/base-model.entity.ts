import { PrimaryGeneratedColumn } from 'typeorm';
import { IsUUID } from 'class-validator';
import { TModelID, IBaseModel } from './base-model.interface';

export abstract class BaseModel implements IBaseModel {
  /**
   * database identification. string uuid by default
   */
  @IsUUID()
  @PrimaryGeneratedColumn('uuid')
  id: TModelID;
}
