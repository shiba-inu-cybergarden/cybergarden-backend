export declare type TModelID = string;

export interface IBaseModel {
  readonly id: TModelID;
}
