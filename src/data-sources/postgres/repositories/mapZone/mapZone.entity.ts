import { Column, Entity } from 'typeorm';
import { BaseModel } from '../../base-model.entity';

@Entity({ name: 'map_zones' })
export class MapZoneModelEntity extends BaseModel {
  @Column('float', { name: 'coordinates', array: true })
  coordinates!: [number[]];

  @Column({ name: 'floor', nullable: false })
  floor!: number;

  @Column({ name: 'zone_name', nullable: false })
  zoneName!: string;
}
