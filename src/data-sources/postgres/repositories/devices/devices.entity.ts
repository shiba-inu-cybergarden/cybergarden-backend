import { Column, Entity } from 'typeorm';
import { BaseModel } from '../../base-model.entity';

@Entity({ name: 'devices' })
export class DeviceModelEntity extends BaseModel {
  @Column({ name: 'device_name', nullable: false })
  deviceName!: string;

  @Column({ name: 'device_brand_name', nullable: true })
  deviceBrandName?: string;

  @Column({ name: 'device_model', nullable: true })
  deviceModel?: string;

  @Column({ name: 'device_os', nullable: true })
  deviceOS?: string;

  @Column({ name: 'battery_charge', nullable: true })
  batteryCharge?: number;

  @Column('float', { name: 'coordinates', default: [], array: true })
  coordinates?: number[];

  @Column({ name: 'height', nullable: true })
  height?: string;

  @Column({ type: 'text', name: 'user_id_taken_device', nullable: true })
  userIdTakenDevice?: string | null;

  @Column({ type: 'text', name: 'user_name_taken_device', nullable: true })
  userNameTakenDevice?: string | null;

  @Column({ name: 'last_time_device_returned', nullable: true })
  lastTimeDeviceReturned?: string;

  @Column({ name: 'is_available', default: true })
  isAvailable!: boolean;

  @Column({ name: 'device_auth_code', nullable: false })
  deviceAuthCode!: string;

  @Column({ name: 'is_auth_complete', default: false })
  isAuthComplete!: boolean;
}
