import { Column, Entity } from 'typeorm';
import { Matches } from 'class-validator';
import { BaseModel } from '../../base-model.entity';

@Entity({ name: 'users' })
export class UserModelEntity extends BaseModel {
  @Column({ name: 'name', nullable: false })
  name!: string;

  @Column({ name: 'email', nullable: false, unique: true })
  email!: string;

  @Column({ name: 'password' })
  @Matches(/((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, { message: 'Пароль слишком простой' })
  password!: string;
}
