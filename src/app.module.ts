import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService, DevicesModule, EnvEnums, mapZoneModule, UsersModule } from './modules';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          entities: ['dist/**/*.entity.js'],
          ssl: configService.config.env !== EnvEnums.TESTS,
          extra:
            configService.config.env !== EnvEnums.TESTS
              ? {
                  ssl: {
                    rejectUnauthorized: false,
                    ca: configService.config.db.driverExtra,
                  },
                }
              : {},
          ...configService.config.db,
          dropSchema: false,
          synchronize: true,
        };
      },
    }),
    UsersModule,
    DevicesModule,
    mapZoneModule,
  ],
})
export class AppModule {}
